netbox-backup
=========

Converted the script and instructions found in the blog [Daily Netbox Database Backup](https://questionable.one/post/netbox-backup/) to an ansible role.

1. Creates a backup directory
2. Deploy a script to the backup directory
3. Creates and enables a couple of systemd items to maintian the backups
4. Schedules the backups for once daily.


Requirements
------------


Role Variables
--------------

The following are default values for the role

```yaml
postgres_user: netbox
netbox_backup_dir: /opt/netbox/backup
netbox_owner: root
netbox_group: root
```

Dependencies
------------


Example Playbook
----------------

```yaml
---
- name: Backup the netbox database
  hosts: netbox

  roles:
    - netbox-backup
```


License
-------

BSD | MIT

Author Information
------------------

Raymond Cox
