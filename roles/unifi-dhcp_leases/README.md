unifi-dhcp_leases
=========

Cat the output of the dnsmasq.lease file on a UDM Pro device and convert to a list of dictionaries for use with things like Netbox IPAM.

Creates a dummy host called `unifi` to make it easier to use with other host playbooks.

Requirements
------------

The UDM Pro device must be configured for SSH access. See the [UniFi - Login with SSH](https://help.ui.com/hc/en-us/articles/204909374-UniFi-Login-with-SSH-Advanced-) page for instructions.

**Important:** Make sure to either include the ansible_password in a vault file, or call `ansible-playbook` with the `-k` flag to provide at runtime.


Role Variables
--------------
```yaml
ansible_user: root
ansible_password: #Place in vault
unifi_dhcp_leases: /mnt/data/udapi-config/dnsmasq.lease
```

Dependencies
------------


Example Playbook
----------------
```yaml
- name: Example pulling DHCP leases from a UDM Pro device
  hosts: unifi
  roles:
    - unifi-dhcp_leases
  tasks:
    - name: Show contents of leases
      debug:
        msg: "{{ hostvars['unifi'].leases }}"


- name: Add IP addresses to netbox that were handed out by unifi
  hosts: netbox
  tasks:
    - name: Ensure the DHCP IP address is recorded in Netbox
      netbox.netbox.netbox_ip_address:
        netbox_url: "{{ netbox_url }}"
        netbox_token: "{{ netbox_token }}"
        validate_certs: "no"
        data:
          address: "{{ item.ip + '/24' }}"
        state: present
      with_items: "{{ hostvars['unifi'].leases }}"
```

License
-------

BSD,MIT

Author Information
------------------

Raymond Cox 
