#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from ansible.errors import AnsibleError, AnsibleFilterError, AnsibleFilterTypeError
from ansible.module_utils.common.text.converters import to_native

"""
  expire         mac             ip       device_name        client_id
1671804898 a0:29:19:d2:b1:08 10.10.0.181 ADMHN-24SZCK3 01:a0:29:19:d2:b1:08
1671784317 00:24:e4:70:35:bc 192.168.98.95 * *
1671764100 d8:a3:5c:46:52:f5 192.168.2.246 Samsung 01:d8:a3:5c:46:52:f5
1671832568 72:0b:a1:6e:1d:7f 192.168.2.195 * 01:72:0b:a1:6e:1d:7f
"""

def unifi_dhcp(dhcp_list):
    """Parse the DHCP client lease file from a Unifi UDM Pro controller into a dictionary
    Args: file content as a list
    Returns:
        List of dictionary entries:
            expire:      Epoch time the entry expires,
            mac:         MAC address of the client
            ip:          IP address assigned by the DHCP server
            device_name: Name provided by the client (* if not provided),
            client_id:   Identifier generated based on the MAC address
    """    
    dhcp_leases = []
    dhcp_list = dhcp_list.split('\n')
    for line in dhcp_list:
        try:
            expire,mac,ip,device_name,client_id = line.split()
            info = {
                    'ip': ip,
                    'expire': expire,
                    'mac': mac,
                    'device_name': device_name,
                    'client_id': client_id
                    }
            dhcp_leases.append(info)
        except Exception as e:
            raise AnsibleFilterError("unifi_dhcp - %s" % to_native(e), orig_exc=e)
    return dhcp_leases


class FilterModule(object):
    filter_map = {
            'unifi_dhcp': unifi_dhcp,
            }
    def filters(self):
        return dict(self.filter_map)
