netbox-config
=========

This role uses a list of values to create a base configuration that could then be extended with other tools. 

Requirements
------------


Role Variables
--------------

```yaml
---
# defaults file for roles/netbox-config
netbox_url: 
netbox_token:
sites:
  - name: 
    status:
    time_zone:
    description:
    physical_address:
    shipping_address:
    comments:
    state: present
contacts:
  - contact_name:
    contact_phone:
    contact_email:
manufacturers:
  - name: 
    description:
    state: present
platforms:
  - name: 
    state: 
ipam_roles:
  - name: 
    state:
vlans:
  - name:
    vid:
    status:
    vlan_role:
    description:
    state:
prefixes:
  - prefix:
    vlan:
      name:
    status: Active
    prefix_role:
    description:
    state: present
rirs:
  - name: RFC 1918
    is_private: True
    state: present
  - name: ARIN
    is_private: False
    state: present
aggregates:
  - prefix: 10.10.0.0/24
    rir: RFC 1918
    state: present
  - prefix: 192.168.0.0/24
    rir: RFC 1918
    state: present

```


Dependencies
------------


Example Playbook
----------------

```yaml
- name: Configure a base netbox installation
  hosts: netbox
  become: yes

  roles:
    - netbox-config

```

License
-------

BSD | MIT

Author Information
------------------

Raymond Cox
